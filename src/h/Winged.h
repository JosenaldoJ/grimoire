/**
 * @file
 * Definition of the Winged class
 */

#ifndef __WINGED_H__
#define __WINGED_H__

#include <string>

#include "Monster.h"

/**
 * @brief      Class for wingeds.
 */
class Winged : public Monster
{

private:
	/** @brief The current agility **/
	int agility;

	/** @brief The original agility **/
	int originalAgility;

public:
	/**
	 * @brief      Constructor.
	 *
	 * @param[in]  id        The id
	 * @param[in]  hp        The original HP
	 * @param[in]  phyAtk    The physical attack
	 * @param[in]  magAtk    The magical attack
	 * @param[in]  agiAtk    The agile atyack
	 * @param[in]  phyDef    The physical defense
	 * @param[in]  magDef    The magical defense
	 * @param[in]  agiDef    The agile defense
	 * @param[in]  agility   The original
	 * @param[in]  nickname  The nickname
	 */
	Winged(int id, int hp, int phyAtk, int magAtk, int agiAtk, int phyDef,
		int magDef, int agiDef, int agility, std::string nickname);

	/**
	 * @brief      Gets the original agility.
	 *
	 * @return     The original agility.
	 */
	int getOriginalAgility();

	/**
	 * @brief      Gets the current agility.
	 *
	 * @return     The current agility.
	 */
	int getAgility();

	/**
	 * @brief      Sets the current agility.
	 *
	 * @param[in]  agility  The current agility
	 */
	void setAgility(int agility);

	/**
	 * @brief      Restores the winged to its original state
	 */
	void restore();

};


#endif