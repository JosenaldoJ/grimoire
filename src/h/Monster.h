/**
 * @file
 * Definition of the Monster class
 */

#ifndef __MONSTER_H__
#define __MONSTER_H__

#include <string> //std::string
#include <ostream> //std::ostream

/**
 * @brief      Class for monsters.
 */
class Monster
{

protected:
	/** @brief The identifier **/
	int id;

	/** @brief The current life **/
	int hp;

	/** @brief The original life **/
	int originalHp;

	/** @brief The physical attack **/
	int phyAtk;

	/** @brief The magical attack **/
	int magAtk;

	/** @brief The agile attack **/
	int agiAtk;

	/** @brief The physical defense **/
	int phyDef;

	/** @brief The magical defense **/
	int magDef;

	/** @brief The agile defense **/
	int agiDef;

	/** @brief The type **/
	char type;

	/** @brief The nickname **/
	std::string nickname;

	/** @brief Next monster in the grimoire **/
	Monster* nextMonster;

public:
	/** @brief Maximum length of the nickname **/
	static const unsigned int MAX_NICKNAME_SIZE;

	/**
	 * @brief      Gets the id.
	 *
	 * @return     The id.
	 */
	int getId();

	/**
	 * @brief      Gets the current HP.
	 *
	 * @return     The current HP.
	 */
	int getHp();

	/**
	 * @brief      Gets the original hp.
	 *
	 * @return     The original hp.
	 */
	int getOriginalHp();

	/**
	 * @brief      Gets the physical attack.
	 *
	 * @return     The physical attack.
	 */
	int getPhyAtk();

	/**
	 * @brief      Gets the magical attack.
	 *
	 * @return     The magical attack.
	 */
	int getMagAtk();	

	/**
	 * @brief      Gets the agile attack.
	 *
	 * @return     The agile attack.
	 */
	int getAgiAtk();

	/**
	 * @brief      Gets the physical defense.
	 *
	 * @return     The physical defense.
	 */
	int getPhyDef();

	/**
	 * @brief      Gets the magical defense.
	 *
	 * @return     The magical defense.
	 */
	int getMagDef();

	/**
	 * @brief      Gets the agile defense.
	 *
	 * @return     The agile defense.
	 */
	int getAgiDef();

	/**
	 * @brief      Gets the type.
	 *
	 * @return     The type.
	 */
	char getType();

	/**
	 * @brief      Gets the nickname.
	 *
	 * @return     The nickname.
	 */
	std::string getNickname();

	/**
	 * @brief      Gets the next monster in the grimoire.
	 *
	 * @return     The next monster in the grimoire.
	 */
	Monster* getNextMonster();

	/**
	 * @brief      Sets the id.
	 *
	 * @param[in]  id    The id
	 */
	void setId(int id);

	/**
	 * @brief      Sets the current hp.
	 *
	 * @param[in]  hp    The current hp
	 */
	void setHp(int hp);

	/**
	 * @brief      Sets the physical attack.
	 *
	 * @param[in]  phyAtk  The physical atack
	 */
	void setPhyAtk(int phyAtk);

	/**
	 * @brief      Sets the magical attack.
	 *
	 * @param[in]  magAtk  The magical attack
	 */
	void setMagAtk(int magAtk);

	/**
	 * @brief      Sets the agile attack
	 *
	 * @param[in]  agiAtk  The agile attack
	 */
	void setAgiAtk(int agiAtk);

	/**
	 * @brief      Sets the physical defense.
	 *             
	 * @param[in]  phyDef  The physical defense
	 */
	void setPhyDef(int phyDef);

	/**
	 * @brief      Sets the magical defense.
	 *
	 * @param[in]  magDef  The magical defense
	 */
	void setMagDef(int magDef);

	/**
	 * @brief      Sets the agile defense.
	 *
	 * @param[in]  agiDef  The agile defense
	 */
	void setAgiDef(int agiDef);

	/**
	 * @brief      Sets the type.
	 *
	 * @param[in]  type  The type
	 */
	void setType(char type);

	/**
	 * @brief      Sets the nickname.
	 *
	 * @param[in]  nickname  The nickname
	 */
	void setNickname(std::string nickname);

	/**
	 * @brief      Sets the next monster in the grimoire.
	 *
	 * @param      nextMonster  The next monster in the grimoire
	 */
	void setNextMonster(Monster* nextMonster);

	/**
	 * @brief      Overrides the << operator for monsters
	 *
	 * @param      o        The output stream
	 * @param      monster  The monster 
	 *
	 * @return     An output stream
	 */
	friend std::ostream& operator<< (std::ostream &o, Monster &monster);

	/**
	 * @brief      Determines if the monster is dead.
	 *
	 * @return     True if the monster is dead, False otherwise.
	 */
	bool isDead();

	/**
	 * @brief      Restore the monster to its original state
	 */
	void restore();
};


#endif