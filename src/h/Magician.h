/**
 * @file
 * Definition of the Magician class
 */

#ifndef __MAGICIAN_H__
#define __MAGICIAN_H__


#include "Monster.h"

/**
 * @brief      Class for magicians.
 */
class Magician : public Monster
{

private:
	/** @brief The magic points **/
	int magicPoint;

	/** @brief The original magic points**/
	int originalMagicPoint;

public:
	/**
	 * @brief      Constructor.
	 *
	 * @param[in]  id          The id
	 * @param[in]  hp          The HP stat
	 * @param[in]  phyAtk      The physical attack
	 * @param[in]  magAtk      The magical attack
	 * @param[in]  agiAtk      The agile attack
	 * @param[in]  phyDef      The physical defense
	 * @param[in]  magDef      The magical defense
	 * @param[in]  agiDef      The agile defense
	 * @param[in]  magicPoint  The magic points
	 * @param[in]  nickname    The nickname
	 */
	Magician(int id, int hp, int phyAtk, int magAtk, int agiAtk, int phyDef,
		int magDef, int agiDef, int magicPoint, std::string nickname);

	/**
	 * @brief      Gets the original magic points.
	 *
	 * @return     The original magic points.
	 */
	int getOriginalMagicPoint();

	/**
	 * @brief      Gets the current magic point..
	 *
	 * @return     The current magic points.
	 */
	int getMagicPoint();

	/**
	 * @brief      Sets the magic points.
	 *
	 * @param[in]  magicPoint  The magic points.
	 */
	void setMagicPoint(int magicPoint);

	/**
	 * @brief      Restores the magician to its original stats
	 */
	void restore();

};


#endif