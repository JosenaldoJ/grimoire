/**
 * @file 
 * Definition of the input/output features
 */

#ifndef __FILEIO_H__
#define __FILEIO_H__

#include <string> ///std::string

#include "Grimoire.h"

/**
 * @brief      Reads a file.
 *
 * @param[in]  path           The path to the file.
 * @param      numberOfLines  The variable where the number of lines will be stored.
 *
 * @return     The content of the file.
 */
std::string* readFile(std::string path, int& numberOfLines);

/**
 * @brief      Writes in a text file.
 *
 * @param[in]  dest           The file where the content will be written in.
 * @param[in]  content        The content which will be written.
 * @param[in]  numberOfLines  The number of lines of the content.
 */
void writeFile(std::string dest, std::string* content, int numberOfLines);

/**
 * @brief      Saves the grimoire in a file.
 *
 * @param      grimoire  The grimoire
 * @param[in]  path      The path to the file
 */
void saveData(Grimoire* grimoire, std::string path);


#endif