/**
 * @file
 * Definition of the Beast class
 */

#ifndef __BEAST_H__
#define __BEAST_H__

#include <string>

#include "Monster.h"

/**
 * @brief      Class for beasts.
 */
class Beast : public Monster
{

private:
	/** @brief The fury of the beast **/
	int fury;

	/** @brief The original fury of the beast **/
	int originalFury;

public:
	/**
	 * @brief      Constructor.
	 *
	 * @param[in]  id        The identifier
	 * @param[in]  hp        The HP
	 * @param[in]  phyAtk    The physical attack
	 * @param[in]  magAtk    The magical attack
	 * @param[in]  agiAtk    The agile attack
	 * @param[in]  phyDef    The physical defense
	 * @param[in]  magDef    The magical defense
	 * @param[in]  agiDef    The agile defense
	 * @param[in]  fury      The fury
	 * @param[in]  nickname  The nickname
	 */
	Beast(int id, int hp, int phyAtk, int magAtk, int agiAtk, int phyDef,
		int magDef, int agiDef, int fury, std::string nickname);

	/**
	 * @brief      Gets the original fury.
	 *
	 * @return     The original fury.
	 */
	int getOriginalFury();

	/**
	 * @brief      Gets the current fury.
	 *
	 * @return     The current fury.
	 */
	int getFury();

	/**
	 * @brief      Sets the fury.
	 *
	 * @param[in]  fury  The fury
	 */
	void setFury(int fury);

	/**
	 * @brief      Determines if the beast has fury.
	 *
	 * @return     True if the beast has fury, False otherwise.
	 */
	bool hasFury();

	/**
	 * @brief      Restores the beast to its original state.
	 */
	void restore();

};


#endif