/**
 * @file
 * Definition of the Grimoire class
 */

#ifndef __GRIMOIRE_H__
#define __GRIMOIRE_H__

#include <string> //std::string

#include "Monster.h"
#include "Beast.h"
#include "Magician.h"
#include "Winged.h"

/**
 * @brief      Class for grimories.
 */
class Grimoire
{

private:
	/** @brief Number of monsters in the grimoire **/
	int size;

	/** @brief Sentinel to the beginning of the grimoire **/
	Monster* head;

	/** @brief Sentinel to the end of the grimoire **/
	Monster* tail;

public:
	/** @brief Maximum number of monsters in a grimoire to start a game **/
	static const int MAX_MONSTERS_START;

	/**
	 * @brief      Constructor.
	 *
	 * @param      monsterList       The list of monsters retrieved from the file.
	 * @param[in]  monsterListLines  The number of lines of the file.
	 */
	Grimoire(std::string* monsterList, int monsterListLines);

	/**
	 * @brief      Destructor.
	 */
	~Grimoire();

	/**
	 * @brief      Gets the head of the grimoire.
	 *
	 * @return     The head of the grimoire.
	 */
	Monster* getHead();

	/**
	 * @brief      Gets the tail of the grimoire.
	 *
	 * @return     The tail of the grimoire.
	 */
	Monster* getTail();

	/**
	 * @brief      Gets the number of monsters in the grimoire.
	 *
	 * @return     The number of monsters in the grimoire.
	 */
	int getSize();

	/**
	 * @brief      Determines if the grimoire is empty.
	 *
	 * @return     True if the grimoire is empty, False otherwise.
	 */
	bool isEmpty();

	/**
	 * @brief      Gets a random monster of the grimoire.
	 *
	 * @return     A random monster of the grimoire.
	 */
	Monster* get();

	/**
	 * @brief      Gets the monster at a specific index.
	 *
	 * @param[in]  i     The index.
	 *
	 * @return     The i-th monster in the grimoire.
	 */
	Monster* get(int i);

	/**
	 * @brief      Gets a monster by its id.
	 *
	 * @param[in]  id    The id.
	 *
	 * @return     The monster which has that id.
	 */
	Monster* getMonsterById(int id);

	/**
	 * @brief      Inserts a monster in the grimoire.
	 *
	 * @param      monster        The monster to be inserted.
	 * @param[in]  calcultatesId  Need of recalculating the id of the monster.
	 */
	void push(Monster* monster, bool calcultatesId = false);

	/**
	 * @brief      Removes a monster.
	 *
	 * @param      monster  The monster to be removed.
	 */
	void removeMonster(Monster* monster);

	/**
	 * @brief      Overrides the << operator for grimoires
	 *
	 * @param      o         Output stream
	 * @param      grimoire  The grimoire 
	 *
	 * @return     Returns a output stream
	 */
	friend std::ostream& operator<< (std::ostream &o, Grimoire &grimoire);

};


#endif
