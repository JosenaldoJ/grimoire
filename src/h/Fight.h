/**
 * @file
 * Definition of the Fight class
 */

#ifndef __FIGHT_H__
#define __FIGHT_H__

/** @brief Constant for using the special power **/
#define USE_SPECIAL "1"

/** @brief Constant for decreasing the accuracy **/
#define DECREASE_ACC "2"

/** @brief Constant for raising the HP **/
#define RAISE_HP "3"

#include "Monster.h"

/**
 * @brief      Class for fight.
 */
class Fight
{

private:
	/** @brief The user's monster **/
	Monster* userMonster;

	/** @brief The computer's monster **/
	Monster* compMonster;

	/** @brief The winner monster **/
	Monster* winner;
	
	/**
	 * @brief      Performs an attack.
	 *
	 * @param      attMonster  The monster which is attacking
	 * @param      defMonster  The monster which is being attacked
	 * @param[in]  acc         The accuracy of the attack
	 * @param[in]  attackType  The type of the attack
	 */
	void attack(Monster* attMonster, Monster* defMonster, float acc, char attackType);

	/**
	 * @brief      Calculates the damage of the attack.
	 *
	 * @param[in]  atkStat   The attack stat
	 * @param[in]  defStat   The defense stat
	 * @param[in]  multiply  The multiplier
	 * @param[in]  acc       The accuracy
	 *
	 * @return     The damage.
	 */
	int calculateDamage(int atkStat, int defStat, float multiply, float acc);

	/**
	 * @brief      Removes the special boosts of the monster.
	 *
	 * @param      monster         The monster whose boosts will be removed
	 * @param      usedFury        If the monster has used its furt
	 * @param      usedMagicPoint  If the monster has used its magic points
	 * @param      usedAgility     If the monster has used its agility
	 */
	void removeSpecialBoost(Monster* monster, bool& usedFury, bool& usedMagicPoint, bool& usedAgility);

	/**
	 * @brief      Prints on the terminal the passive actions
	 */
	void passiveActionsMessage();

public:
	/**
	 * @brief      Constructor
	 *
	 * @param      userMonster  The user's monster
	 * @param      compMonster  The computer's monster
	 */
	Fight(Monster* userMonster, Monster* compMonster);

	/**
	 * @brief      Gets the winner of the fight.
	 *
	 * @return     The winner of the fight.
	 */
	Monster* getWinner();

	/**
	 * @brief      Starts the fight.
	 */
	void begin();
};

#endif