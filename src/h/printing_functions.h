/**
 * @file
 * Definitions of functions that print stuff on the terminal
 */

#ifndef __PRINTING_FUNCTIONS_H__
#define __PRINTING_FUNCTIONS_H__

#include "Monster.h"

void emptySpecialErrorMessage(Monster* monster);

#endif