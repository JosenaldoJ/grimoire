/**
 * @file
 * Main file of the project
 */

#include <iostream> //std::cin, std::computer
#include <ctime> //time
#include <cstdlib> // srand, rand   
#include <algorithm> //std::all_of 

#include "../h/file_io.h"
#include "../h/Fight.h"

/**
 * @brief      Main function of the program
 *
 * @return     Return code
 */
int main()
{
	srand(time(NULL));

	int userGrimoireTxtSize;
	int compGrimoireTxtSize;

	std::string* userGrimoireLines = readFile("../data/user_grimoire.txt", userGrimoireTxtSize);
	std::string* compGrimoireLines = readFile("../data/comp_grimoire.txt", compGrimoireTxtSize);

	Grimoire* userGrimoire = new Grimoire(userGrimoireLines, userGrimoireTxtSize);
	Grimoire* compGrimoire = new Grimoire(compGrimoireLines, compGrimoireTxtSize);

	if(userGrimoire->isEmpty())
	{
		std::cout << "ERROR: empty user's grimoire" << std::endl;

		delete[] userGrimoireLines;
		delete[] compGrimoireLines;
		delete userGrimoire;
		delete compGrimoire;
		return(0);
	}

	if(compGrimoire->isEmpty())
	{
		std::cout << "ERROR: empty computer's grimoire" << std::endl;

		delete[] userGrimoireLines;
		delete[] compGrimoireLines;
		delete userGrimoire;
		delete compGrimoire;
		return(0);
	}

	
	std::string possibleIdUserMonster;

	std::cout << "\nYou have the following monsters in your Grimoire:\n\n";
	std::cout << *userGrimoire;
	std::cout << "Type the id of the monster which will fight: ";
	std::cin >> possibleIdUserMonster;

	//Verifies if the id typed by the user is a valid integer
	if(possibleIdUserMonster.empty() || !std::all_of(possibleIdUserMonster.begin(), possibleIdUserMonster.end(), ::isdigit))
	{
		std::cout << "ERROR: invalid ID (" << possibleIdUserMonster << ")\n";

		delete[] userGrimoireLines;
		delete[] compGrimoireLines;
		delete userGrimoire;
		delete compGrimoire;
		return(0);
	}

	//Builds user's monster
	Monster* userMonster = userGrimoire->getMonsterById(std::stoi(possibleIdUserMonster));

	if(userMonster == nullptr)
	{
		std::cout << "ERROR: no monster with ID = " << possibleIdUserMonster << "\n";

		delete[] userGrimoireLines;
		delete[] compGrimoireLines;
		delete userGrimoire;
		delete compGrimoire;
		return(0);
	}

	std::cout << "\nYou chose " << userMonster->getNickname() << "\n\n";

	//Picks a random monster of the computer's grimoire
	Monster* compMonster = compGrimoire->get();
	
	std::cout << "Computer's monster:\n\n" << *compMonster;

	Fight fight = Fight(userMonster, compMonster);
	fight.begin();

	Monster* genericWinner = fight.getWinner();

	compMonster->restore();
	userMonster->restore();

	if(genericWinner == userMonster)
	{
		std::cout << genericWinner->getNickname() << " won the fight!!!!!!!!!!!\n\n";

		compGrimoire->removeMonster(compMonster);
		userGrimoire->push(compMonster, true);
	}

	else if(genericWinner == compMonster)
	{
		std::cout << genericWinner->getNickname() << " won the fight!!!!!!!!!!!\n\n";

		userGrimoire->removeMonster(userMonster);
		compGrimoire->push(userMonster, true);
	}

	else
	{
		std::cout << "No winners\n";
	}

	delete[] compGrimoireLines;
	delete[] userGrimoireLines;

	saveData(userGrimoire, "../data/user_grimoire.txt");
	saveData(compGrimoire, "../data/comp_grimoire.txt");

	delete userGrimoire;
	delete compGrimoire;

	return 0;
}