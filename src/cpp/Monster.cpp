/**
 * @file
 * Implementation of the Monster class
 */

#include "../h/Monster.h"
#include "../h/Magician.h"
#include "../h/Beast.h"
#include "../h/Winged.h"

/*static*/
const unsigned int Monster::MAX_NICKNAME_SIZE = 20;

int Monster::getId()
{
	return this->id;
}

int Monster::getHp()
{
	return this->hp;
}

int Monster::getOriginalHp()
{
	return this->originalHp;
}

int Monster::getPhyAtk()
{
	return this->phyAtk;
}

int Monster::getMagAtk()
{
	return this->magAtk;
}

int Monster::getAgiAtk()
{
	return this->agiAtk;
}

int Monster::getPhyDef()
{
	return this->phyDef;
}

int Monster::getMagDef()
{
	return this->magDef;
}

int Monster::getAgiDef()
{
	return this->agiDef;
}

char Monster::getType()
{
	return this->type;
}

std::string Monster::getNickname()
{
	return this->nickname;
}

Monster* Monster::getNextMonster()
{
	return this->nextMonster;
}

void Monster::setId(int id)
{
	this->id = id;
}

void Monster::setHp(int hp)
{
	this->hp = hp;
}

void Monster::setPhyAtk(int phyAtk)
{
	this->phyAtk = phyAtk;
}

void Monster::setMagAtk(int magAtk)
{
	this->magAtk = magAtk;
}

void Monster::setAgiAtk(int agiAtk)
{
	this->agiAtk = agiAtk;
}

void Monster::setPhyDef(int phyDef)
{
	this->phyDef = phyDef;
}

void Monster::setMagDef(int magDef)
{
	this->magDef = magDef;
}

void Monster::setAgiDef(int agiDef)
{
	this->agiDef = agiDef;
}

void Monster::setType(char type)
{
	this->type = type;
}

void Monster::setNickname(std::string nickname)
{
	this->nickname = nickname;
}

void Monster::setNextMonster(Monster* nextMonster)
{
	this->nextMonster = nextMonster;
}

std::ostream& operator<< (std::ostream& o, Monster& monster)
{
	o << "ID: " << monster.getId() << '\n';
	o << "Type: ";

	switch(monster.getType())
	{
		case 'm':
			o << "Magician" << '\n';
			break;

		case 'b':
			o << "Beast" << '\n';
			break;

		case 'w':
			o << "Winged" << '\n';
			break;

		default:

			break;
	}

	o << "Nickname: " << monster.getNickname() << '\n';
	o << "HP: " << monster.getHp() << '\n';
	o << "Physical Attack: " << monster.getPhyAtk() << '\n';
	o << "Magical Attack: " << monster.getMagAtk() << '\n';
	o << "Agile Attack: " << monster.getAgiAtk() << '\n';
	o << "Physical Defense: " << monster.getPhyDef() << '\n';
	o << "Magical Defense: " << monster.getMagDef() << '\n';
	o << "Agile Defense: " << monster.getAgiDef() << '\n';

	if(monster.getType() == 'm')
	{
		Magician* magician = static_cast<Magician*>(&monster);
		o << "Magic points: " << magician->getMagicPoint() << "\n\n";
	}

	else if(monster.getType() == 'b')
	{
		Beast* beast = static_cast<Beast*>(&monster);
		o << "Fury: " << beast->getFury() << "\n\n";
	}

	else
	{
		Winged* winged = static_cast<Winged*>(&monster);
		o << "Agility: " << winged->getAgility() << "\n\n";
	}

	return o;
}

bool Monster::isDead()
{
	return this->hp == 0;
}

void Monster::restore()
{
	this->hp = this->originalHp;

	if(this->type == 'w')
	{
		Winged* tempCopy = static_cast<Winged*>(this);
		tempCopy->setAgility(tempCopy->getOriginalAgility());
	}

	else if(this->type == 'm')
	{
		Magician* tempCopy = static_cast<Magician*>(this);
		tempCopy->setMagicPoint(tempCopy->getOriginalMagicPoint());
	}

	else
	{
		Beast* tempCopy = static_cast<Beast*>(this);
		tempCopy->setFury(tempCopy->getOriginalFury());
	}
}