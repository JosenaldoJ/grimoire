/**
 * @file
 * Implementation of the Beast class
 */

#include "../h/Beast.h"

Beast::Beast(int id, int hp, int phyAtk, int magAtk, int agiAtk, int phyDef,
		int magDef, int agiDef, int fury, std::string nickname)
{
	this->id = id;
	this->originalHp = hp;
	this->hp = hp;
	this->phyAtk = phyAtk;
	this->magAtk = magAtk;
	this->agiAtk = agiAtk;
	this->phyDef = phyDef;
	this->magDef = magDef;
	this->agiDef = agiDef;
	this->originalFury = fury;
	this->fury = fury;
	this->nickname = nickname;
}

int Beast::getOriginalFury()
{
	return this->originalFury;
}

int Beast::getFury()
{
	return this->fury;
}

void Beast::setFury(int fury)
{
	this->fury = fury;
}

bool Beast::hasFury()
{
	return this->fury != 0;
}

void Beast::restore()
{
	this->hp = originalHp;
	this->fury = originalFury;
}