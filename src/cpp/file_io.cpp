/**
 * @file
 * Implementation of the input/output features
 */

#include <fstream> ///std::ifstream, std::ofstream
#include <iostream> ///std::cout, std::endl

#include "file_io.h"

std::string* readFile(std::string path, int& numberOfLines)
{
	int maxNumberOfLines = (Grimoire::MAX_MONSTERS_START)*11;
	int contLines = 0;

	///Stores partially the content of each line
	std::string line;

	///Pointer to the source file
	std::ifstream file;

	///Stores the entire read text
	std::string* dest = new std::string[maxNumberOfLines];

	file.open(path);

	if(file.is_open())
	{

		while(std::getline(file, line) && contLines < maxNumberOfLines)
		{
			dest[contLines++] = line;
		}

		numberOfLines = contLines;
		file.close();
	}

	else
	{
		numberOfLines = 0;
		std::cout << "FAILURE WHEN READING FILE " << path << std::endl;
	}

	return dest;
}

void writeFile(std::string dest, std::string* content, int numberOfLines)
{
	///Pointer to the file
	std::ofstream file(dest);

	if(file.is_open())
	{
		for(int i = 0; i < numberOfLines; ++i)
		{
			file << content[i];

			if(i+1 != numberOfLines)
			{
				file << '\n';
			}
		}
		
		file.close();
	}

	else
	{
		std::cout << "UNABLE TO OPEN/CREATE FILE " << dest << std::endl;
	}

	delete[] content;
}

void saveData(Grimoire* grimoire, std::string src)
{
	std::string* grimoireLines = new std::string[grimoire->getSize()*11];
	Monster* iterator = grimoire->getHead()->getNextMonster();

	int cont = 0;

	while(iterator != grimoire->getTail())
	{
		grimoireLines[cont++] = std::to_string(iterator->getId());
		grimoireLines[cont++] = std::to_string(iterator->getHp());
		grimoireLines[cont++] = std::to_string(iterator->getPhyAtk());
		grimoireLines[cont++] = std::to_string(iterator->getMagAtk());
		grimoireLines[cont++] = std::to_string(iterator->getAgiAtk());
		grimoireLines[cont++] = std::to_string(iterator->getPhyDef());
		grimoireLines[cont++] = std::to_string(iterator->getMagDef());
		grimoireLines[cont++] = std::to_string(iterator->getAgiDef());
		
		if(iterator->getType() == 'm')
		{
			Magician* tempCopy = static_cast<Magician*>(iterator);

			grimoireLines[cont++] = std::to_string(tempCopy->getMagicPoint());
			grimoireLines[cont++] = tempCopy->getNickname();
			grimoireLines[cont++] = tempCopy->getType();
		}

		else if(iterator->getType() == 'b')
		{
			Beast* tempCopy = static_cast<Beast*>(iterator);

			grimoireLines[cont++] = std::to_string(tempCopy->getFury());
			grimoireLines[cont++] = tempCopy->getNickname();
			grimoireLines[cont++] = tempCopy->getType();
		}

		else
		{
			Winged* tempCopy = static_cast<Winged*>(iterator);

			grimoireLines[cont++] = std::to_string(tempCopy->getAgility());
			grimoireLines[cont++] = tempCopy->getNickname();
			grimoireLines[cont++] = tempCopy->getType();
		}

		iterator = iterator->getNextMonster();
	}

	writeFile(src, grimoireLines, cont);
}