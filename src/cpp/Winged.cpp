/**
 * @file
 * Implementation of the Winged class
 */

#include "../h/Winged.h"

Winged::Winged(int id, int hp, int phyAtk, int magAtk, int agiAtk, int phyDef,
		int magDef, int agiDef, int agility, std::string nickname)
{
	this->id = id;
	this->originalHp = hp;
	this->hp = hp;
	this->phyAtk = phyAtk;
	this->magAtk = magAtk;
	this->agiAtk = agiAtk;
	this->phyDef = phyDef;
	this->magDef = magDef;
	this->agiDef = agiDef;
	this->originalAgility = agility;
	this->agility = agility;
	this->nickname = nickname;
}

int Winged::getOriginalAgility()
{
	return this->originalAgility;
}

int Winged::getAgility()
{
	return this->agility;
}

void Winged::setAgility(int agility)
{
	this->agility = agility;
}

void Winged::restore()
{
	this->hp = this->originalHp;
	this->agility = this->originalAgility;
}