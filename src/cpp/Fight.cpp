/**
 * @file
 * Implementation of the Fight class
 */

#include <iostream> //std::cin, std::cout
#include <cstdlib> //rand
#include <cmath> //ceil

#include "../h/Fight.h"
#include "../h/Magician.h"
#include "../h/Beast.h"
#include "../h/Winged.h"

Fight::Fight(Monster* userMonster, Monster* compMonster)
{
	this->userMonster = userMonster;
	this->compMonster = compMonster;
	this->winner = nullptr;
}

Monster* Fight::getWinner()
{
	return this->winner;
}

void Fight::begin()
{
	std::cout << "LET THE FIGHT BEGIN!!!111!!ONZE!\n\n";

	std::string userPassMove;
	int compPassMove;

	bool userUsedMagicPoint = false;
	bool userUsedFury = false;
	bool userUsedAgility = false;

	bool compUsedMagicPoint = false;
	bool compUsedFury = false;
	bool compUsedAgility = false;

	float userAcc = 1;
	float compAcc = 1;

	std::string possibleUserAttackType;
	char userAttackType;
	char compAttackType;

	int newHp;

	while(!this->compMonster->isDead() && !this->userMonster->isDead())
	{
		std::cout << compMonster->getNickname() << " has " << compMonster->getHp() << " hp\n";
		std::cout << userMonster->getNickname() << " has " << userMonster->getHp() << " hp\n\n";
		this->passiveActionsMessage();

		std::cin >> userPassMove;

		//Apply user's passive move

		if(userPassMove == USE_SPECIAL)
		{
			if(this->userMonster->getType() == 'm')
			{
				Magician* tempCopy = static_cast<Magician*>(this->userMonster);

				if(tempCopy->getMagicPoint() != 0)
				{
					std::cout << userMonster->getNickname() << " is charging its Magic Points...\n";
					userUsedMagicPoint = true;

					this->userMonster->setMagAtk(this->userMonster->getMagAtk() + tempCopy->getMagicPoint());
					tempCopy->setMagicPoint(0);
				}

				else
				{
					std::cout << userMonster->getNickname() << " runned out of Magic Points!!!\n\n";
				}
			}

			else if(userMonster->getType() == 'b')
			{
				Beast* tempCopy = static_cast<Beast*>(this->userMonster);

				if(tempCopy->getFury() != 0)
				{
					std::cout << userMonster->getNickname() << " is charging its Fury...\n";
					userUsedFury = true;

					this->userMonster->setPhyAtk(this->userMonster->getPhyAtk() + tempCopy->getFury());
					tempCopy->setFury(0);
				}

				else
				{
					std::cout << userMonster->getNickname() << " runned out of Fury!!!\n\n";
				}
			}

			else
			{
				Winged* tempCopy = static_cast<Winged*>(this->userMonster);

				if(tempCopy->getAgility() != 0)
				{
					std::cout << userMonster->getNickname() << " is charging its Agility...\n";
					userUsedAgility = true;

					this->userMonster->setAgiAtk(this->userMonster->getAgiAtk() + tempCopy->getAgility());
					tempCopy->setAgility(0);
				}

				else
				{
					std::cout << userMonster->getNickname() << " runned out of Agility!!!\n\n";
				}
			}
		}

		else if(userPassMove == DECREASE_ACC)
		{
			compAcc = 0.75;
		}

		else if(userPassMove == RAISE_HP)
		{
			if(userMonster->getHp() < userMonster->getOriginalHp()/2)
			{
				newHp = userMonster->getHp() + userMonster->getOriginalHp()/20;	

				if(newHp > userMonster->getOriginalHp()/2)
				{
					newHp = userMonster->getOriginalHp() / 2;
				}

				std::cout << userMonster->getNickname() << " raised its HP by " << newHp - userMonster->getHp()	 << "\n";

				userMonster->setHp(newHp);
			}
		}

		else
		{
			std::cout << "\nERROR: invalid action\n\n";

			continue;
		}

		//Gets the types of the active moves

		std::cout << "Choose an active move (p for physical attack, m for magical attack, a for agile attack): ";
		std::cin >> possibleUserAttackType;

		if(possibleUserAttackType != "p" && possibleUserAttackType != "m" && possibleUserAttackType != "a")
		{
			std::cout << "\nERROR: invalid action\n\n";
			
			continue;
		}

		else
		{
			userAttackType = possibleUserAttackType[0];
		}

		std::cout << "\n";

		if(compMonster->getType() == 'w')
		{
			compAttackType = 'a';
		}

		else if(compMonster->getType() == 'b')
		{
			compAttackType = 'p';
		}

		else 
		{
			compAttackType = 'm';
		}

		//Apply computer's passive move
		
		compPassMove = rand()%3 + 1;

		if(std::to_string(compPassMove) == RAISE_HP)
		{
			if(compMonster->getHp() < compMonster->getOriginalHp()/2)
			{
				newHp = compMonster->getHp() + compMonster->getOriginalHp()/20;	

				if(newHp > compMonster->getOriginalHp()/2)
				{
					newHp = compMonster->getOriginalHp() / 2;
				}

				std::cout << compMonster->getNickname() << " raised its HP by " << newHp - compMonster->getHp() << "\n";

				compMonster->setHp(newHp);
			}
		}

		else if(std::to_string(compPassMove) == DECREASE_ACC)
		{
			userAcc = 0.75;
		}

		else
		{
			// If the computer's used its special already but its current passive move is 
			// using the special, then the passive move becomes decreasing user's accuracy
			
			if(this->compMonster->getType() == 'm')
			{
				Magician* tempCopy = static_cast<Magician*>(this->compMonster);

				if(tempCopy->getMagicPoint() != 0)
				{
					std::cout << compMonster->getNickname() << " is charging its Magic Points...\n";
					compUsedMagicPoint = true;

					this->compMonster->setMagAtk(this->compMonster->getMagAtk() + tempCopy->getMagicPoint());
					tempCopy->setMagicPoint(0);
				}

				else
				{
					userAcc = 0.75;
				}
			}

			else if(compMonster->getType() == 'b')
			{
				Beast* tempCopy = static_cast<Beast*>(this->compMonster);

				if(tempCopy->getFury() != 0)
				{
					std::cout << compMonster->getNickname() << " is charging its Fury...\n";
					compUsedFury = true;

					this->compMonster->setPhyAtk(this->compMonster->getPhyAtk() + tempCopy->getFury());
					tempCopy->setFury(0);
				}

				else
				{
					userAcc = 0.75;
				}
			}

			else
			{
				Winged* tempCopy = static_cast<Winged*>(this->compMonster);

				if(tempCopy->getAgility() != 0)
				{
					std::cout << compMonster->getNickname() << " is charging its Agility...\n";
					compUsedAgility = true;

					this->compMonster->setAgiAtk(this->compMonster->getAgiAtk() + tempCopy->getAgility());
					tempCopy->setAgility(0);
				}

				else
				{
					userAcc = 0.75;
				}
			}
		}


		//Performs the attacks
		this->attack(compMonster, userMonster, compAcc, compAttackType);
		if(!userMonster->isDead()) this->attack(userMonster, compMonster, userAcc, userAttackType);
		

		//Remove temporary effects
		this->removeSpecialBoost(this->userMonster, userUsedFury, userUsedMagicPoint, userUsedAgility);
		this->removeSpecialBoost(this->compMonster, compUsedFury, compUsedMagicPoint, compUsedAgility);
		userAcc = 1;
		compAcc = 1;
	}

	if(this->compMonster->isDead())
	{
		this->winner = this->userMonster;
	}

	else
	{
		this->winner = this->compMonster;
	}
}

void Fight::attack(Monster* attMonster, Monster* defMonster, float acc, char attackType)
{
	std::cout << attMonster->getNickname() << " attacked " << defMonster->getNickname() << " using its ";
	float boostByTypeAdvantage;

	//Monsters' attack and defense stats to be used in the damage calc
	int atkStat;
	int defStat;

	if(attackType == 'm')
	{
		std::cout << "Magical Force!!!!\n";

		atkStat = attMonster->getMagAtk();
		defStat = defMonster->getMagDef();

		if(defMonster->getType() == 'm')
		{
			boostByTypeAdvantage = 1;
		}

		else if(defMonster->getType() == 'w')
		{
			boostByTypeAdvantage = 2;
		}

		else
		{
			boostByTypeAdvantage = 0.5;
		}
	} 

	else if(attackType == 'a')
	{
		std::cout << "Agile Force!!!!\n";

		atkStat = attMonster->getAgiAtk();
		defStat = defMonster->getAgiDef();

		if(defMonster->getType() == 'm')
		{
			boostByTypeAdvantage = 0.5;
		}

		else if(defMonster->getType() == 'w')
		{
			boostByTypeAdvantage = 1;
		}

		else
		{
			boostByTypeAdvantage = 2;	
		}
	}

	else
	{
		std::cout << "Physical Force!!!!\n";

		atkStat = attMonster->getPhyAtk();
		defStat = defMonster->getPhyDef();

		if(defMonster->getType() == 'm')
		{
			boostByTypeAdvantage = 4;
		}

		else if(defMonster->getType() == 'w')
		{
			boostByTypeAdvantage = 0.25;
		}

		else
		{
			boostByTypeAdvantage = 1;
		}
	}

	int damage = this->calculateDamage(atkStat, defStat, boostByTypeAdvantage, acc);

	if(damage == 0)
	{
		std::cout << attMonster->getNickname() << "'s attack didn't affect " << defMonster->getNickname() << "!!!\n";
	}

	else if(damage >= defMonster->getHp())
	{
		std::cout << attMonster->getNickname() << " KILLED " << defMonster->getNickname() << "!!!!!!!!!\n";

		damage = defMonster->getHp();
	}

	else
	{
		std::cout << defMonster->getNickname() << " lost " << damage << " hp\n";	
	}

	defMonster->setHp(defMonster->getHp() - damage);

	std::cout << "\n";
}

int Fight::calculateDamage(int atkStat, int defStat, float multiply, float acc)
{
	int attackTry = rand()%10 + 1;

	if(attackTry > acc*10)
	{
		return 0;
	}

	else
	{
		if(defStat == 0) ++defStat;

		return ceil((atkStat*1.0/defStat) * 10 * multiply);
	}
}

void Fight::removeSpecialBoost(Monster* monster, bool& usedFury, bool& usedMagicPoint, bool& usedAgility)
{
	if(usedFury)
	{	
		Beast* tempCopy = static_cast<Beast*>(monster);
		monster->setPhyAtk(monster->getPhyAtk() - tempCopy->getOriginalFury());

		usedFury = false;
	}

	else if(usedAgility)
	{
		Winged* tempCopy = static_cast<Winged*>(monster);
		monster->setAgiAtk(monster->getAgiAtk() - tempCopy->getOriginalAgility());

		usedAgility = false;
	}

	else if(usedMagicPoint)
	{	
		Magician* tempCopy = static_cast<Magician*>(monster);
		monster->setMagAtk(monster->getMagAtk() - tempCopy->getOriginalMagicPoint());


		usedMagicPoint = false;
	}
}

void Fight::passiveActionsMessage()
{
	std::cout << "Passive actions:\n\n";
	std::cout << "(1*) Use special\n";
	std::cout << "(2*) Decrease opponent's accuracy by 20%\n";
	std::cout << "(3) Raises current hp by 5% of the original hp (up to the half of the original)\n";
	std::cout << "*The effect of the action lasts only the turn in which the action is performed\n\n";
	std::cout << "Choose a passive action: ";
}