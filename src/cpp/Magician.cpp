/**
 * @file
 * Implementation of the Magician class
 */

#include "../h/Magician.h"

Magician::Magician(int id, int hp, int phyAtk, int magAtk, int agiAtk, int phyDef,
		int magDef, int agiDef, int magicPoint, std::string nickname)
{
	this->id = id;
	this->originalHp = hp;
	this->hp = hp;
	this->phyAtk = phyAtk;
	this->magAtk = magAtk;
	this->agiAtk = agiAtk;
	this->phyDef = phyDef;
	this->magDef = magDef;
	this->agiDef = agiDef;
	this->originalMagicPoint = magicPoint;
	this->magicPoint = magicPoint;
	this->nickname = nickname;
}

int Magician::getOriginalMagicPoint()
{
	return this->originalMagicPoint;	
}

int Magician::getMagicPoint()
{
	return this->magicPoint;
}

void Magician::setMagicPoint(int magicPoint)
{
	this->magicPoint = magicPoint;
}

void Magician::restore()
{
	this->hp = originalHp;
	this->magicPoint = originalMagicPoint;
}