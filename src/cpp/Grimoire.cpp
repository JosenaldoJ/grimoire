/**
 * @file
 * Implementation of the Grimoire class
 */

#include <iostream> //std::cout, std::endl
#include <algorithm> //std::all_of, std::sort
#include <cstdlib> // rand   

#include "../h/Grimoire.h"

const int Grimoire::MAX_MONSTERS_START = 15;

Grimoire::Grimoire(std::string* monsterList, int monsterListLines)
{
	this->head = new Monster();
	this->tail = new Monster();	
	this->head->setNextMonster(this->tail);
	this->size = 0;

	int lineIterator = -1;
	int contMonsters = 0;

	std::string possibleHp;
	std::string possibleSpecial;
	std::string possiblePhyAtk;
	std::string possibleMagAtk;
	std::string possibleAgiAtk;
	std::string possiblePhyDef;
	std::string possibleMagDef;
	std::string possibleAgiDef;

	int id;
	int hp;
	int special;
	int phyAtk;
	int magAtk;
	int agiAtk;
	int phyDef;
	int magDef;
	int agiDef;

	std::string nickname;
	std::string type;


	while(lineIterator+11 < monsterListLines)
	{
		if(monsterList[++lineIterator].empty())
		{
			id = contMonsters++;
		}

		else
		{
			if(!std::all_of(monsterList[lineIterator].begin(), monsterList[lineIterator].end(), ::isdigit))
			{
				std::cout << "Invalid id: " << monsterList[lineIterator] << std::endl;		

				break;
			}

			else
			{
				id = std::stoi(monsterList[lineIterator]);
			}
		}

		possibleHp = monsterList[++lineIterator];

		//Checks if it is a number
		if(possibleHp.empty() || !std::all_of(possibleHp.begin(), possibleHp.end(), ::isdigit))
		{
			std::cout << "Invalid hp: " << possibleHp << std::endl;

			break;
		}

		else
		{
			hp = std::stoi(monsterList[lineIterator]);
		}

		possiblePhyAtk = monsterList[++lineIterator];

		//Checks if it is a number
		if(possiblePhyAtk.empty() || !std::all_of(possiblePhyAtk.begin(), possiblePhyAtk.end(), ::isdigit))
		{
			std::cout << "Invalid phyAtk: " << possiblePhyAtk << std::endl;

			break;
		}

		else
		{
			phyAtk = std::stoi(monsterList[lineIterator]);
		}

		possibleMagAtk = monsterList[++lineIterator];

		//Checks if it is a number
		if(possibleMagAtk.empty() || !std::all_of(possibleMagAtk.begin(), possibleMagAtk.end(), ::isdigit))
		{
			std::cout << "Invalid magAtk: " << possibleMagAtk << std::endl;

			break;
		}

		else
		{
			magAtk = std::stoi(monsterList[lineIterator]);
		}

		possibleAgiAtk = monsterList[++lineIterator];

		//Checks if it is a number
		if(possibleAgiAtk.empty() || !std::all_of(possibleAgiAtk.begin(), possibleAgiAtk.end(), ::isdigit))
		{
			std::cout << "Invalid agiAtk: " << possibleAgiAtk << std::endl;

			break;
		}

		else
		{
			agiAtk = std::stoi(monsterList[lineIterator]);
		}

		possiblePhyDef = monsterList[++lineIterator];

		//Checks if it is a number
		if(possiblePhyDef.empty() || !std::all_of(possiblePhyDef.begin(), possiblePhyDef.end(), ::isdigit))
		{
			std::cout << "Invalid phyDef: " << possiblePhyDef << std::endl;

			break;
		}

		else
		{
			phyDef = std::stoi(monsterList[lineIterator]);
		}

		possibleMagDef = monsterList[++lineIterator];

		//Checks if it is a number
		if(possibleMagDef.empty() || !std::all_of(possibleMagDef.begin(), possibleMagDef.end(), ::isdigit))
		{
			std::cout << "Invalid magDef: " << possibleMagDef << std::endl;

			break;
		}

		else
		{
			magDef = std::stoi(monsterList[lineIterator]);
		}

		possibleAgiDef = monsterList[++lineIterator];

		//Checks if it is a number
		if(possibleAgiDef.empty() || !std::all_of(possibleAgiDef.begin(), possibleAgiDef.end(), ::isdigit))
		{
			std::cout << "Invalid agiDef: " << possibleAgiDef << std::endl;

			break;
		}

		else
		{
			agiDef = std::stoi(monsterList[lineIterator]);
		}

		possibleSpecial = monsterList[++lineIterator];

		//Checks if it is a number
		if(possibleSpecial.empty() || !std::all_of(possibleSpecial.begin(), possibleSpecial.end(), ::isdigit))
		{
			std::cout << "Invalid special stat: " << possibleSpecial << std::endl;

			break;
		}

		else
		{
			special = std::stoi(monsterList[lineIterator]);
		}

		nickname = monsterList[++lineIterator];

		if(nickname.size() > Monster::MAX_NICKNAME_SIZE)
		{
			std::cout << "Invalid nickname: " << nickname << std::endl;

			break;
		}

		type = monsterList[++lineIterator];

		Monster* newMonster;

		if(type.compare("w") == 0)
		{
			newMonster = new Winged(id, hp, phyAtk, magAtk, agiAtk, phyDef, magDef, agiDef, special, nickname);
			newMonster->setType('w');
		}

		else if(type.compare("m") == 0)
		{
			newMonster = new Magician(id, hp, phyAtk, magAtk, agiAtk, phyDef, magDef, agiDef, special, nickname);
			newMonster->setType('m');
		}

		else if(type.compare("b") == 0)
		{
			newMonster = new Beast(id, hp, phyAtk, magAtk, agiAtk, phyDef, magDef, agiDef, special, nickname);
			newMonster->setType('b');
		}

		else
		{
			std::cout << "Invalid type: " << type << std::endl;

			break;	
		}

		this->push(newMonster);
	}

}

Grimoire::~Grimoire()
{
	Monster* aux = this->head;

	while(this->head != this->tail)
	{
		this->head = this->head->getNextMonster();

		delete aux;

		aux = this->head;
	}

	delete this->tail;
}

Monster* Grimoire::getHead()
{
	return this->head;
}

Monster* Grimoire::getTail()
{
	return this->tail;
}

int Grimoire::getSize()
{
	return this->size;
}

bool Grimoire::isEmpty()
{
	return this->size == 0;
}

Monster* Grimoire::get(int i)
{
	if(i > this->size || i < 1)
	{
		return nullptr;
	}

	else
	{
		Monster* aux = this->head;

		while(i > 0)
		{
			--i;
			aux = aux->getNextMonster();
		}

		return aux;
	}
}

Monster* Grimoire::get()
{
	if(this->isEmpty())
	{
		return nullptr;
	}

	else
	{
		int index = rand()%this->size + 1;
		
		Monster* aux = this->head;

		while(index > 0)
		{
			aux = aux->getNextMonster();
			--index;			
		}

		return aux;
	}
}

Monster* Grimoire::getMonsterById(int id)
{
	Monster* aux = this->head->getNextMonster();

	while(aux != this->tail)
	{
		if(aux->getId() == id)
		{
			return aux;
		}

		aux = aux->getNextMonster();
	}

	return nullptr;
}

void Grimoire::push(Monster* monster, bool calcultatesId)
{
	if(calcultatesId)
	{
		// The logic consists of verifying if there is an "id jump" in the grimoire
		// (e.g. a monster with ID = 6, a monster with ID = 8 but no monster with
		// ID = 7). In this case, the id of the new monster should be a "jumped id"

		// Put all currents id in a list which will be sorted
		int* listOfIds = new int[this->size];
		Monster* aux = this->head;

		for(int i = 0; i < this->size; ++i)
		{
			aux = aux->getNextMonster();
			listOfIds[i] = aux->getId();
		}

		std::sort(listOfIds, listOfIds + this->size);

		// Look for a jumped id
		
		if(listOfIds[0] != 0)
		{
			monster->setId(0);
		}

		else
		{
			int expectedId = -1;

			for(int i = 0; i < this->size; ++i)
			{
				if(i != listOfIds[i])
				{
					expectedId = i;

					break;
				}
			}

			// There are no jumped ids
			if(expectedId == -1)
			{
				expectedId = this->size;
			}

			monster->setId(expectedId);
		}

		delete[] listOfIds;

	}

	monster->setNextMonster(this->head->getNextMonster());

	this->head->setNextMonster(monster);
	++this->size;
}

void Grimoire::removeMonster(Monster* monster)
{
	if(this->size > 0)
	{
		if(monster == this->head->getNextMonster())
		{
			this->head->setNextMonster(monster->getNextMonster());
			--this->size;
		}

		else
		{
			Monster* auxRight = this->head->getNextMonster();
			Monster* auxLeft = auxRight;

			while(auxRight != this->tail)
			{
				if(auxRight == monster)
				{
					auxLeft->setNextMonster(auxRight->getNextMonster());

					--this->size;

					break;
				}

				auxLeft = auxRight;
				auxRight = auxRight->getNextMonster(); 
			}
			
		}
	}
}


std::ostream& operator<< (std::ostream& o, Grimoire& grimoire)
{
	Monster* it = grimoire.getHead()->getNextMonster();

	while(it != grimoire.getTail())
	{
		o << (*it);
		it = it->getNextMonster();
	}

	return o;
}

