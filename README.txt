CONFIGURAÇÃO

	Os grimórios localizam-se na pasta /data/. O grimório do usuário está armazenado no 
	arquivo "user_grimoire.txt" e o do computador, no "comp_grimoire.txt". No mesmo 
	diretório, há, ainda, os arquivos de backup "user_grimoire_copy.txt" e 
	"comp_grimoire_copy.txt". O programa não restora automaticamente o estado original
	dos grimórios quando um é esvaziado. Logo, caso deseje-se jogar outra partida com 
	a mesma configuração original, deve-se copiar manualmente os grimórios dos arquivos
	de backup. 

	Cada monstro nos arquivos de grimório deve ter o seguinte formato:

	(linha em branco onde será armazenado o id, que é calculado pelo programa)
	HP
	Physical Attack
	Magical Attack
	Agile Attack
	Physical Defense
	Magical Defense
	Agile Defense
	Especial
	Nickname 
	Tipo (um char cujo valor é 'm' se o monstro é Magician, 'w' se é Winged, 'b' se é Beast)
	
	A separação entre dois monstros é dada por UMA quebra de linha.

COMPILAÇÃO

	É necessário entrar na pasta raiz do projeto e digitar "make" (caso haja arquivos .o, 
	deve-se digitar antes "make clean"). Na compilação, foi usado o g++ 4.7.3.

EXECUÇÃO

	Deve-se entrar no subdiretório /bin/ e digitar "./exe"

FEATURES

	Uso de alocação dinâmica de memória	
		
		(Rodar com o valgrind para verificar que a memória está sendo desalocada, pois
		cada desalocação não ocorre necessariamente no mesmo arquivo onde foi feita a
		alocação correspondente)

		Grimoire.cpp:16
		Grimoire.cpp:17
		Grimoire.cpp:204
		Grimoire.cpp:210
		Grimoire.cpp:216
		Grimoire.cpp:338
		file_io.cpp:23
		file_io.cpp:78
		main.cpp:29
		main.cpp:30

	Sobrecarga de funções

		Grimoire.cpp:268
		Grimoire.cpp:289

	Lista 
		
		(A classe Grimoire consiste numa lista encadeada de monstros)
		Grimoire.cpp

	Sobrecarga de operadores

		Grimoire.cpp:424
		Monster.cpp:129
